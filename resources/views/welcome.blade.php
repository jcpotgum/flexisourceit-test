@extends('layout.mainlayout')
    @section('content')
       <div class="album text-muted">
         <div class="container">
           <div class="row" ng-controller="PlayersCtrl">
                <table class='table'>
                    <thead>
                    <tr>
                        <th>Player Id</th>
                        <th>Player Name</th>
                    </tr>
                    </thead>
                    <tbody id="players-list">
                    
                    </tbody>
                </table>
           </div>
         </div>
       </div>
       <div class="modal" id="player-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Player Details</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <table class="table">
                <tr>
                  <td class="text-left">Player ID :</td>
                  <td class="player-id text-center"></td>
                </tr>
                <tr>
                  <td class="text-left">Player First Name :</td>
                  <td class="player-first-name text-center"></td>
                </tr>
                <tr>
                  <td class="text-left">Player Last Name :</td>
                  <td class="player-last-name text-center"></td>
                </tr>
                <tr>
                  <td class="text-left">Form :</td>
                  <td class="player-form text-center"></td>
                </tr>
                <tr>
                  <td class="text-left">Total Points :</td>
                  <td class="player-total-points text-center"></td>
                </tr>
                <tr>
                  <td class="text-left">Influence :</td>
                  <td class="player-influence text-center"></td>
                </tr>
                <tr>
                  <td class="text-left">Creativity :</td>
                  <td class="player-creativity text-center"></td>
                </tr>
                <tr>
                  <td class="text-left">Threat :</td>
                  <td class="player-threat text-center"></td>
                </tr>
                <tr>
                  <td class="text-left">ICT Index :</td>
                  <td class="player-ict-index text-center"></td>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </div>

    @endsection