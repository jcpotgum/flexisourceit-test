<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use App\Player;


class PlayerTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_home_page(){
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function test_api_all_players(){
        $response = $this->json('GET', 'api/players');

        $response->assertStatus(200);

    }

    public function test_api_player(){
        $player = Player::find(1)->first();
        $id = $player['player_id'];
        $this->assertIsInt($id);

        $response = $this->json('GET', 'api/players/'.$player['player_id']);
        $response->assertStatus(200)->assertJson(['status' => 'ok']);
    }

    public function test_if_api_data_xml(){
        $apiUrl = 'https://www.w3schools.com/xml/note.xml';
        $xml = simplexml_load_file($apiUrl);
        if(is_array($xml)){
            $this->assertTrue(true);
        }else{
            $this->assertFalse(false);
        }
    }

    public function test_if_api_data_json(){
        $apiUrl = 'https://fantasy.premierleague.com/api/bootstrap-static/';
        
        
        $apiData = json_decode(file_get_contents($apiUrl), true);
        if(is_array($apiData)){
            $this->assertTrue(true);
        }else{
            $this->assertFalse(false);
        }

    }

    public function test_importer_function_url(){
        $response = $this->get('/get_players_from_api');

        $response->assertStatus(200);

    }

    public function test_insert_to_players(){
        $player = new Player;

        $player->player_id = '999';
        $player->full_name = 'Test Name';
        $player->details = array('properties' => 'values' );

        $this->assertTrue($player->save());
    }

    public function test_update_player(){
        $updated = Player::where('player_id', '999')
            ->update([
                'full_name' => 'New Name',
            ]);

            $this->assertGreaterThan(0, $updated);
    }
}
