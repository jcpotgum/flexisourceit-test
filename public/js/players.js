
var playerList = $('#players-list');
var playerModal = $('#player-modal');

$(document).ready(function(){
    var players = {};
    var playerDetail = {};
    
    $.ajax({
        url: "http://localhost:8000/api/players/",
        beforeSend : function(){
            playerList.html('');
            var loading = $('<tr class="loading"><td class="text-center" colspan="2">Loading...</td></tr>');
            playerList.html(loading);
        },
        success: function(response){
            renderList(response.details);
        },
        error : function (xhr,status,error){
            var error = $('<tr class="error"><td class="text-center" colspan="2">Opps! Something went wrong.</td></tr>');
            playerList.html(error)
        },
        complete: function(){
            playerList.find('tr.loading').remove();
        }
    });

    $('#players-list').on('click','.detail',function(){
        getDetail($(this));
    });
})


function renderList(players){
    if(players.length > 0){
        //render list
        $.each(players, function(index, value){
            playerList.append($('<tr><td>' + value.player_id + '</td><td><a class="detail" data-id="' + value.player_id + '" href="javascript:void(0)">' + value.full_name + '</a></td></tr>'));
        });
    }else{
        playerList.html($('<tr class="error"><td class="text-center" colspan="2">No Records Found.</td></tr>'));
    }
}


function getDetail(detail){
    $.ajax({
        url: "http://localhost:8000/api/players/" + detail.data('id'),
        success: function(response){
           showModal(response.details);
        },
        error : function (xhr,status,error){
            console.log(xhr,status,error);
        },
        complete: function(){
            console.log('complete');
        }
    });
}


function showModal(response){
    playerModal.find('td.player-first-name').text(response.details.first_name);
    playerModal.find('td.player-last-name').text(response.details.second_name);
    playerModal.find('td.player-id').text(response.player_id);
    playerModal.find('td.player-form').text(response.details.form);
    playerModal.find('td.player-total-points').text(response.details.total_points);
    playerModal.find('td.player-influence').text(response.details.influence);
    playerModal.find('td.player-creativity').text(response.details.creativity);
    playerModal.find('td.player-threat').text(response.details.threat);
    playerModal.find('td.player-ict-index').text(response.details.ict_index);
    playerModal.modal('show')
}