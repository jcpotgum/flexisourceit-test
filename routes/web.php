<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/get_players_from_api','PlayersController@index');

Route::get('/api/test', function () {
    return json_encode(['test' => '123']);
});

Route::get('/api/players', 'PlayersController@getAllPlayers');