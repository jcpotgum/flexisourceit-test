<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
    //

    protected $table = 'players';

    protected $fillable = ['player_id','full_name', 'details'];

    public $timestamps = false;

    protected $casts = [
        'details' => 'array',
    ];


}
