<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Player;

class PlayersController extends Controller
{
    public function index()
    {
        $apiUrl = 'https://fantasy.premierleague.com/api/bootstrap-static/';
        
        $apiData = json_decode(file_get_contents($apiUrl), true);

        //check if data is JSON or XML
        if(!$apiData){
            //API is XML
            $xml = simplexml_load_file($apiUrl);

            $json = json_encode($xml);

            $apiData = json_decode($json,TRUE);
        }

        $playerData = $apiData['elements'];

        if(count($playerData) > 0){
            for($i = 0; $i < 100; $i++){
                $this->savePlayer($playerData[$i]);
            }
        }
    }

    private function savePlayer($playerData){
        $player = Player::updateOrCreate(
            ['player_id' => $playerData['id']],
            [
                'details' => $playerData,
                'player_id' => $playerData['id'],
                'full_name' => $playerData['first_name'].' '.$playerData['second_name']
            ]
        );
    }



    public function getAllPlayers(){
        $players = Player::get(['player_id', 'full_name']);
        $headers = ['Content-Type' => 'application/json; charset=UTF-8'];
        $response = [
            'status' => 'ok',
            'details' => $players
        ];
        return response()->json($response, 200, $headers);
    }

    public function getPlayer($id){
        if (Player::where('id', $id)->exists()) {
            $player = Player::where('player_id', $id)->get()->first();
            $response = [
                'status' => 'ok',
                'details' => $player
            ];
            $headers = ['Content-Type' => 'application/json; charset=UTF-8'];
            return response()->json($response, 200, $headers);
          } else {
            return response()->json([
              "message" => "Player not found"
            ], 400, $headers);
          }
    }
}